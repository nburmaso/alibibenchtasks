#!/bin/bash

# determine commits
export ALIDISTCOMMIT=${ALIPERF_ALIDISTCOMMIT}
export O2COMMIT=${ALIPERF_O2COMMIT}

# run the workflow
export NEVENTS=${NEVENTS:-20}
export SYSTEM=${SYSTEM:-PbPb}
export ENGINE=${ENGINE:-TGeant3}
export INTERACTIONRATE=${INTERACTIONRATE:-50}

CONFIG="${SYSTEM}${NEVENTS}${ENGINE}RATE${INTERACTIONRATE}"

today=`date +%d-%m-%Y-%H:%m`

TARGETDIR=simrecoworkflow_${today}_${CONFIG}_ALIDIST:${ALIDISTCOMMIT}_O2:${O2COMMIT}
mkdir ${TARGETDIR}

if [[ -d "${TARGETDIR}" ]]
then
  cd ${TARGETDIR}

  # launch the job
  ${O2_ROOT}/prodtests/sim_challenge.sh -s ${SYSTEM:-pp} -n ${NEVENTS:-100} -e ${ENGINE:-TGeant3} ${INTERACTIONRATE:+-r ${INTERACTIONRATE}}
  RETURN_CODE=$?
  echo "RETURN CODE ${RETURN_CODE}"  

  [ ! ${RETURN_CODE} -eq 0 ] && return ${RETURN_CODE}

  # remove empty files
  find . -name '*' -size 0 -exec rm {} ';'

  eos mkdir /eos/user/a/aliperf/simulation/${TARGETDIR}
  eos cp * /eos/user/a/aliperf/simulation/${TARGETDIR}
fi
