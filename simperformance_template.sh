#!/bin/bash

# determine commits
export ALIDISTCOMMIT=${ALIPERF_ALIDISTCOMMIT}
export O2COMMIT=${ALIPERF_O2COMMIT}

# run the benchmark
${O2_ROOT}/prodtests/sim_performance_test.sh ${NEVENTS:-100} ${GEN:-pythia8pp}
RETURN_CODE=$?
echo "RETURN CODE ${RETURN_CODE}"  

[ ! ${RETURN_CODE} -eq 0 ] && return ${RETURN_CODE}

# send benchmark data to Influx (use framework function)
# (metrics.dat is produced by sim_performance_test)
submit_to_AliPerf_InfluxDB metrics.dat

today=`date +%d-%m-%Y-%H:%m`

# tar up the build artefacts and send them to EOS
cd ${DIR}

TARFILE=artefacts_${SYSTEM:-pp}_perf_${today}_ALIDIST:${ALIDISTCOMMIT}_O2:${O2COMMIT}.tar.gz

# tar up everything and sent to EOS
tar --force-local -czf ${TARFILE} *

eos cp ${TARFILE} /eos/user/a/aliperf/simulation/O2SimBenches/
